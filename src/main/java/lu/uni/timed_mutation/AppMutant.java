package lu.uni.timed_mutation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import lu.uni.timed_mutation.model.Model;
import lu.uni.timed_mutation.mutation.ConstraintLargerLocation;
import lu.uni.timed_mutation.mutation.ConstraintLargerTransition;
import lu.uni.timed_mutation.mutation.ConstraintNegationTransition;
import lu.uni.timed_mutation.mutation.ConstraintSmallerLocation;
import lu.uni.timed_mutation.mutation.ConstraintSmallerTransition;
import lu.uni.timed_mutation.mutation.Mutation;
import lu.uni.timed_mutation.mutation.Mutator;
import lu.uni.timed_mutation.mutation.StateMissing;
import lu.uni.timed_mutation.mutation.TransitionAddedControllable;
import lu.uni.timed_mutation.mutation.TransitionAddedUncontrollable;
import lu.uni.timed_mutation.mutation.TransitionMissing;


/*
(1) the SUT shall not be redundant; 

(2) TMI shall not be applied to uncontrollable transitions;

(3) TAD shall not be applied to controllable transitions; 

(4) SMI shall not be applied when all incoming transitions are uncontrollable;

(5) CXL-T shall not be applied to controllable transitions with guards of the form x<=k;
(6) CXL-T shall not be applied to uncontrollable transitions with guards either (i)
x>=k or (ii) x==k and source invariant x<=k; 

(7) CXL-L can be applied to invariants of the form x<=k;
(8) CXL-L shall not be applied to invariants of the form x>=k whenever all incoming transitions are uncontrollable;

(9) CXS-T shall not be applied to controllable transitions with guards of the form x>=k;
(10) CXS-T shall not be applied to uncontrollable transitions with guards of the form x<=k;

(11) CXS-L shall not be applied to invariants of the form x<=k whenever all incoming transitions are uncontrollable;
(12) CXS-L shall not be applied to invariants of the form x>=k whenever all incoming transitions are controllable.
*/

public class AppMutant {
	
	private static File file;
	private static DocumentBuilder docBuilder;
	private static Transformer transformer;
	private static DOMSource source;
	
	private static String fileName = null;
	private static String output = ".";
	private static boolean secondOrder = false;
	private static boolean exhaustive = true;

	public static void main(String argv[]) {

		for(int i = 0; i < argv.length; i++) {
			if(argv[i].equals("-i")) {
				fileName = argv[++i];
			}
			if(argv[i].equals("-o")) {
				output = argv[++i];
			}
			if(argv[i].equals("-secondOrder")) {
				secondOrder = true;
			}
			if(argv[i].equals("-sample")) {
				exhaustive = false; 
			}
		}
		
		if(!output.equals("."))
			try {
				Files.createDirectories(Paths.get(output));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		try {

			AppMutant app = new AppMutant();
			file = app.loadFileFromResources(fileName);
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docFactory.newDocumentBuilder();
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();

			//long start = System.currentTimeMillis();
			if(!secondOrder)
				runFirstOrder(app);
			else
				runSecondOrder(app, exhaustive);
			//long end = System.currentTimeMillis();
			//System.out.println(end - start);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Usage : java -jar timed-mutation.jar [options] -i model.xml\n"
					+ "where options are :\n"
					+ "\t-o folder to indicate the folder where mutants will be generated\n"
					+ "\t-secondOrder to generate second order mutants\n"
					+ "\t-sampling to generate samples (10%) of second order mutants");
			return;
		}
	}
	
	private Model original() throws SAXException, IOException {
		
		Document doc = docBuilder.parse(file);
		source = new DOMSource(doc);
		Node root = doc.getFirstChild();
		return new Model(root);
	}
	
	private static void runFirstOrder(AppMutant app) throws TransformerException, SAXException, IOException {
		
		Mutation tmi = new TransitionMissing(); // 88
		Mutation tadc = new TransitionAddedControllable(); // 289
		Mutation tadu = new TransitionAddedUncontrollable(); // 289
		Mutation smi = new StateMissing(); // 17
		Mutation cxlt = new ConstraintLargerTransition(); // 9
		Mutation cxst = new ConstraintSmallerTransition(); // 9
		Mutation cxll = new ConstraintLargerLocation(); // 11
		Mutation cxsl = new ConstraintSmallerLocation(); // 11
		Mutation ccn = new ConstraintNegationTransition(); // 9
		
		Mutation[] mutations = { tmi, tadc, tadu, smi, cxlt, cxst, cxll, cxsl, ccn };
		
		Model m = app.original();
				
		for(int i = 0; i < mutations.length; i++) { 
			int v = app.runOneMutation(mutations[i]);
			System.out.println(mutations[i].name()+","+v);
		}
	}

	private static void runSecondOrder(AppMutant app, boolean exhaustive) throws TransformerException, SAXException, IOException {
		
		Mutation tmi = new TransitionMissing(); // 88
		Mutation tadc = new TransitionAddedControllable(); // 289
		Mutation tadu = new TransitionAddedUncontrollable(); // 289
		Mutation smi = new StateMissing(); // 17
		Mutation cxlt = new ConstraintLargerTransition(); // 9
		Mutation cxst = new ConstraintSmallerTransition(); // 9
		Mutation cxll = new ConstraintLargerLocation(); // 11
		Mutation cxsl = new ConstraintSmallerLocation(); // 11
		Mutation ccn = new ConstraintNegationTransition(); // 9
		
		Mutation[] mutations = { tmi, tadc, tadu, smi, cxlt, cxll, cxst, cxsl, ccn };
		
		int cv = 0;
		
		for(int i = 0; i < mutations.length; i++) 
			for(int j = 0; j < mutations.length; j++) {
				    int v = exhaustive? app.runtwoMutations(mutations[i], mutations[j]): app.runtwoMutationsSampling(mutations[i], mutations[j]);
				    System.out.println(mutations[i].name()+"_"+mutations[j].name()+","+v);
			}
	}

	
	private int runtwoMutationsSampling(Mutation m1, Mutation m2) throws TransformerException, SAXException, IOException {
		
		int v = 0;
		boolean v1 = false;
		boolean v2 = false;
		
		Random rand = new Random(12345);
		
		Model ori = original();
		int n1 = ori.getPossibleMutations(m1);
		int n2 = ori.getPossibleMutations(m2);
				
		int samples_emp = n1 * n2 * 10/100; 
		samples_emp = samples_emp > 1000? (int) ((n1 * n2) * 1/100) : samples_emp;
		
		//System.out.println(samples_emp);
		
		for(int k = 0; k < samples_emp; k++) {
		
			ori = original();
			Mutator mutator_1 = new Mutator(ori);
			int i = rand.nextInt(n1);
			v1 = mutator_1.mutate(m1, i, false);
			
			// Applying the actual two mutations
			//System.out.println("----- second order mutation "+i+"/"+j+" -----");
			
			Model mutant = mutator_1.getMutant();
			int j = rand.nextInt(mutant.getPossibleMutations(m2));
			Mutator mutator_2 = new Mutator(mutant);
			v2 = mutator_2.mutate(m2, j, true);
			
			//if the two mutations violates their guidelines the mutant is redundant?
			if(v1 && v2)
				v++;
			
			ori.addAutomata(mutator_2.getMutant());
			StreamResult result = new StreamResult(storeFileFromResources("mutant_"+m1.name()+"_"+(i+1)+"_"+m2.name()+"_"+(j+1)+".xml"));
			transformer.transform(source, result);
		
		    //System.out.println("mutant_"+m1.name()+"_"+(i+1)+"_"+m2.name()+"_"+(j+1)+" violations = "+v);
		}
		
		return v;
	}
	
    private int runtwoMutations(Mutation m1, Mutation m2) throws TransformerException, SAXException, IOException {
		
		int v = 0;
		boolean v1 = false;
		boolean v2 = false;
		
		Model ori = original();
		int n1 = ori.getPossibleMutations(m1);
		for(int i = 0; i < n1; i++) {
			
			ori = original();
			Mutator mutator_1 = new Mutator(ori);
			mutator_1.mutate(m1, i, false);
			
			int n2 = mutator_1.getMutant().getPossibleMutations(m2);
			
			for(int j = 0; j < n2; j++) {
				
				ori = original();
				mutator_1 = new Mutator(ori);
				v1 = mutator_1.mutate(m1, i, false);					
				
				Model mutant = mutator_1.getMutant();
				Mutator mutator_2 = new Mutator(mutant);
				v2 = mutator_2.mutate(m2, j, false);
				
				if(v1 && v2)
					v++;
				
				ori.addAutomata(mutator_2.getMutant());
				StreamResult result = new StreamResult(storeFileFromResources("mutant_"+m1.name()+"_"+(i+1)+"_"+m2.name()+"_"+(j+1)+".xml"));
				transformer.transform(source, result);
				
				
			}
		}
		
		return v;
	}
	
	private int runOneMutation(Mutation mutation) throws TransformerException, SAXException, IOException {
		
		int number = original().getPossibleMutations(mutation);
		
		int v = 0;
		
		for (int i = 0; i < number; i++) {//original.transitions().size(); i++) {
			// Copying the whole XML tree
		    Model ori = original();
			Mutator mutator = new Mutator(ori);
			if(mutator.mutate(mutation, i, false))
				v++;
			
			ori.addAutomata(mutator.getMutant());
			StreamResult result = new StreamResult(storeFileFromResources("mutant_"+mutation.name()+"_"+(i+1)+".xml"));
			transformer.transform(source, result);
		}
		
		return v;
	}
	
	private File loadFileFromResources(String fileName) throws IOException {
		return new File(fileName);
	}
	
	private File storeFileFromResources(String fileName) {
		try {
			Files.createFile(Paths.get(output+"/"+fileName));
		} catch (IOException e) {
		}
		return new File(output+"/"+fileName);

	}

}