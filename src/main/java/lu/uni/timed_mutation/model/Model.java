package lu.uni.timed_mutation.model;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lu.uni.timed_mutation.mutation.Mutation;

public class Model {
	private static String AUTOMATON = "template";
	private static String LOCATION = "location";
	private static String TRANSITION = "transition";
	private static String DECLARATION = "declaration";
	private static String SYSTEM = "system";
	
	private Node root;
	private Map<String, Node> automata;
	private Node system;
	private String systemList;
	private String systemIOs;
	
	public enum GuardKind{
		EQ,
		GT,
		LT,
		NEQ,
		LEQ,
		GEQ
	}
	
	public Model(Node root) {
		this.root = root;
		this.automata = new HashMap<>();
		List<Node> automataList = children(root, AUTOMATON);
		
		for (Node automaton : automataList) {
			this.automata.put(
					automatonName(automaton).getTextContent(), 
					automaton);
		}
		
		this.system = children(root, SYSTEM).get(0);
		String desc = system.getTextContent();
		this.systemList = desc.substring(
				desc.indexOf(SYSTEM), 
				desc.indexOf(";") + 1);
		

		this.systemIOs = desc.substring(desc.indexOf("IO"));
	}
	
	public Model(Model mutant) {
		root = mutant.root;
		automata = mutant.automata;
		system = mutant.system;
		systemList = mutant.systemList;
		systemIOs = mutant.systemIOs;
	}
	
	public void copy(Model mutant) {
		root = mutant.root;
		automata = mutant.automata;
		system = mutant.system;
		systemList = mutant.systemList;
		systemIOs = mutant.systemIOs;
	}
	
	public void addIO(String chan, String io) {
		
		if(this.systemIOs.indexOf(chan+io) == -1)
			this.systemIOs = this.systemIOs.replace("}", ", "+chan+io+"}");
		
		Node declaration = declaration();
		String strdecl = declaration.getTextContent();
		if(strdecl.indexOf(chan)==-1) {
			declaration.setTextContent(strdecl.substring(strdecl.indexOf("broadcast"), strdecl.indexOf(";")).replace(';', ',')+ ", "+chan+";");
		}
		
		children(root, SYSTEM).get(0).setTextContent(this.systemList+"\n"+systemIOs+"\n");
	}
	
	public void addAutomata(Model mutant) {
		
		Node declaration = declaration();
		declaration.setTextContent(mutant.declaration().getTextContent());
		
		root.removeChild(system);
		
		for (Node automaton : mutant.automata()) {
			String oldName = automatonName(automaton).getTextContent();
			for(Node a : automata())
					changeAutomatonName(automaton);
			String newName = automatonName(automaton).getTextContent();
			//System.out.println("automaton:" +newName);
			root.appendChild(automaton);
			
			this.systemList = 
					systemList.substring(0, systemList.indexOf(';'))
					+ ", "
					+ automatonName(automaton).getTextContent()
					+ ";";
			
			
			this.systemIOs = this.systemIOs.concat((mutant.systemIOs).replace(oldName, newName));
		}
		
		root.appendChild(system);
		children(root, SYSTEM).get(0).setTextContent(this.systemList+"\n"+systemIOs+"\n");
	}
	
	public Node root() {
		return root;
	}
	
	public Node system() {
		return system;
	}
	
	public List<Node> automata() {
		return children(root, AUTOMATON);
	}
	
	public Node declaration() {
		return children(root, DECLARATION).get(0);
	}
	
	public List<Node> locations() {
		List<Node> result = new LinkedList<>();
		
		for (Node automaton : automata.values()) {
			result.addAll(locations(automaton));
		}
		
		return result;
	}
	
	public List<Node> locationsWithoutInitial() {
		List<Node> result = new LinkedList<>();
		
		for (Node automaton : automata.values()) {
			result.addAll(locationsWithoutInitial(automaton));
		}
		
		return result;
	}
	
	public List<Node> transitions() {
		List<Node> result = new LinkedList<>();
		
		for (Node automaton : automata.values()) {
			result.addAll(transitions(automaton));
		}
		
		return result;
	}
	
	public List<Node> locationsWithInvariant() {
		List<Node> result = new LinkedList<>();
		List<Node> locations = locations();
		
        for(Node l : locations) {
            if(invariant(l)!=null)
            	result.add(l);
        }
        
        return result;
	}
	
	public List<Node> locationsWithInvariantGreaterThan(int bound){
		List<Node> result = new LinkedList<>();
		for(Node l : locationsWithInvariant())
			if(Model.bound(invariant(l)) > bound)
				result.add(l);
		return result;
	}
	
	public List<Node> transitionsWithGuard() {
		List<Node> result = new LinkedList<>();
		List<Node> transitions = transitions();
		
		for(Node t : transitions) {
            if(guard(t)!=null)
            	result.add(t);
        }
		
		return result;
	}
	
	public List<Node> transitionsWithGuardGreaterThan(int bound){
		List<Node> result = new LinkedList<>();
		for(Node t : transitionsWithGuard())
			if(Model.bound(guard(t)) > bound)
				result.add(t);
		return result;
	}
	
	public Node getSourceLocation(Node transition) {
		String locId = Model.children(transition, "source").get(0).getAttributes().getNamedItem("ref").getTextContent();
		for(Node loc : locations()) {
			if(loc.getAttributes().getNamedItem("id").getTextContent().equals(locId)) {
				return loc;
			}
		}
		return null;
	}
	
	public Node getTargetLocation(Node transition) {
		String locId = Model.children(transition, "target").get(0).getAttributes().getNamedItem("ref").getTextContent();
		for(Node loc : locations()) {
			if(loc.getAttributes().getNamedItem("id").getTextContent().equals(locId)) {
				return loc;
			}
		}
		return null;
	}
	
	public List<Node> locations(Node automaton) {
		return children(automaton, LOCATION);
	}
	
	public List<Node> locationsWithoutInitial(Node automaton) {
		List<Node> result = new LinkedList<>();
		for(Node l : children(automaton, LOCATION))
			if(!isInitialLocation(automaton,l))
				result.add(l);
		return result;
	}
	
	public boolean isInitialLocation(Node automaton, Node location) {
		String locId = location.getAttributes().getNamedItem("id").getTextContent();
		return locId.contentEquals(Model.children(automaton, "init").get(0).getAttributes().getNamedItem("ref").getTextContent());
	}
	
	public List<Node> transitions(Node automaton) {
		return children(automaton, TRANSITION);
	}
	
	public List<Node> transitionsWithActions() {
		List<Node> result = new LinkedList<>();
		for(Node automaton : automata()) {
			for(Node t : transitions(automaton)) {
				if(action(t)!=null)
					result.add(t);
			}
		}
		return result;
	}
	
	public static List<Node> children(Node node, String childName) {
		List<Node> result = new LinkedList<>();
		
		NodeList children = node.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (child.getNodeName().equals(childName)) {
				result.add(child);
			}
		}
			
		return result;
	}
	
	public Node invariant(Node location) {
		Node result = findAttributedNode(location, "invariant");
		if(result != null && !isComplexGuard(result))
			return result;
		return null;
	}

	public Node guard(Node transition) {
		Node result = findAttributedNode(transition, "guard");
		if(result != null && !isComplexGuard(result))
			return result;
	    return null;
	}
	
	public Node action(Node transition) {
		return findAttributedNode(transition, "synchronisation");
	}
	
	public List<Node> incomingTransitions(Node location) {
		List<Node> res = new LinkedList<>();
		String id = location.getAttributes().getNamedItem("id").getTextContent();
		for(Node t : transitions()) {
			if(id.contentEquals(Model.children(t, "target").get(0).getAttributes().getNamedItem("ref").getTextContent()))
				res.add(t);
		}
		return res;
	}
	
	public boolean incomingTransitionsUncontrollable(Node location) {
		for (Node t : incomingTransitions(location)) {
			if(t.getAttributes().getNamedItem("controllable").getTextContent().equals("true"))
				 return false;
		}
		return true;
	}
	
	public boolean incomingTransitionsControllable(Node location) {
		for (Node t : incomingTransitions(location)) {
			if(t.getAttributes().getNamedItem("controllable").getTextContent().equals("false"))
				 return false;
		}
		return true;
	}
	
	public static GuardKind guardkind(Node guard) {
		String asText = guard.getTextContent();
		if (asText.contains("==")) {
			return GuardKind.EQ;
		} else if (asText.contains("<=")) {
			return GuardKind.LEQ;
		} else if (asText.contains(">=")) {
			return GuardKind.GEQ;
		} else if (asText.contains("<")) {
			return GuardKind.LT;
		} else if (asText.contains(">")) {
			return GuardKind.GT;
		} else {
			assert(false);
		}
		return null;
	}
	
	public static boolean isComplexGuard(Node guard) {
		return guard.getTextContent().contains("&&") || guard.getTextContent().contains("||");
	}
	
	public static int bound(Node constraint) {
		String asText = constraint.getTextContent();

		int index;
		if (asText.contains("==")) {
			index = asText.indexOf("==") + 2;
		} else if (asText.contains("=")) {
			index = asText.indexOf("=") + 1;
		} else if (asText.contains("<")) {
			index = asText.indexOf("<") + 1;
		} else if (asText.contains(">")) {
			index = asText.indexOf(">") + 1;
		} else {
			index = asText.indexOf(";") + 1;
		}
		String boundAsText = asText.substring(index);
		int bound = Integer.parseInt(boundAsText.replaceAll("\\s+",""));
		
		
		return bound;
	}
	
	public static void raiseBound(Node constraint) {
		String asText = constraint.getTextContent();

		int index;
		if (asText.contains("==")) {
			index = asText.indexOf("==") + 2;
		} else if (asText.contains("=")) {
			index = asText.indexOf("=") + 1;
		} else if (asText.contains("<")) {
			index = asText.indexOf("<") + 1;
		} else if (asText.contains(">")) {
			index = asText.indexOf(">") + 1;
		} else {
			index = asText.indexOf(";") + 1;
		}
		String boundAsText = asText.substring(index);
		int bound = Integer.parseInt(boundAsText.replaceAll("\\s+",""));

		int newBound = bound + 1;
		constraint.setTextContent(asText.substring(0, index) + newBound);
	}
	
	public static void reduceBound(Node constraint) {
		String asText = constraint.getTextContent();

		int index;
		if (asText.contains("==")) {
			index = asText.indexOf("==") + 2;
		} else if (asText.contains("=")) {
			index = asText.indexOf("=") + 1;
		} else if (asText.contains("<")) {
			index = asText.indexOf("<") + 1;
		} else if (asText.contains(">")) {
			index = asText.indexOf(">") + 1;
		} else {
			index = asText.indexOf(";") + 1;
		}
		String boundAsText = asText.substring(index);
		int bound = Integer.parseInt(boundAsText.replaceAll("\\s+",""));

		int newBound = bound - 1;
		constraint.setTextContent(asText.substring(0, index) + newBound);
	}

	public static void negate(Node constraint) {
		String asText = constraint.getTextContent();

		String result = null;
		if (asText.contains("==")) {
			result = asText.replace("==", "!=");
		} else if(asText.contains("!=")) {
			result = asText.replace("!=", "==");
		} else if (asText.contains("<=")) {
			result = asText.replace("<=", ">");
		} else if (asText.contains(">=")) {
			result = asText.replace(">=", "<");
		} else if (asText.contains("<")) {
			result = asText.replace("<", ">=");
		} else if (asText.contains(">")) {
			result = asText.replace(">", "<=");
		} else {
			throw new RuntimeException(asText);
		}
		
		constraint.setTextContent(result);
	}
	
	public static Node findAttributedNode(Node root, String attr) {
		Node result = null;
		
		
		NodeList list = root.getChildNodes();

		for (int i = 0; result == null && i < list.getLength(); i++) {

			Node node = list.item(i);

			if ("label".equals(node.getNodeName())) {
				if (attr.equals(node.getAttributes().getNamedItem("kind").getTextContent())) {
					if (node.getTextContent() != null && !node.getTextContent().isEmpty()) {
						result = node;
					}
				}
			}
		}
		
		return result;
	}
	
	private Node automatonName(Node automaton) {
		
		NodeList list = automaton.getChildNodes();

		for (int i = 0; i < list.getLength(); i++) {

			Node node = list.item(i);

			if ("name".equals(node.getNodeName())) {
				return node;
			}
		}
		
		return null;
	}
	
	private void changeAutomatonName(Node automaton) {
		Node name = automatonName(automaton);
		name.setTextContent(name.getTextContent() + "_mutant");
	}
	
	public String asText(Node transition) {
		String source = Model.children(transition, "source").get(0).getAttributes().getNamedItem("ref").getTextContent();
		String target = Model.children(transition, "target").get(0).getAttributes().getNamedItem("ref").getTextContent();
		return source + " -> " + target + " / " +transition.getTextContent();
	}

	public int getPossibleMutations(Mutation mutation) {
		return mutation.getNumberOfMutations(this);
	}

	public void removeTransitions(Node location) {
		Node automaton = automata().get(0);
		List<Node> transitions = transitions(automaton);
		String locId = location.getAttributes().getNamedItem("id").getTextContent();
		for (int j = 0; j < transitions.size(); j++) {
			Node transition = transitions.get(j);
			if (locId.contentEquals(Model.children(transition, "source").get(0).getAttributes().getNamedItem("ref").getTextContent()) 
					|| locId.contentEquals(Model.children(transition, "target").get(0).getAttributes().getNamedItem("ref").getTextContent())) {
				automaton.removeChild(transition);
			}
		}
		
	}
	
}
