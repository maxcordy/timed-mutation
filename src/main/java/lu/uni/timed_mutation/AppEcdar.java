package lu.uni.timed_mutation;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 
 * @author Davide Basile
 * @author Maxime Cordy
 *
 */
public class AppEcdar {

	public enum MutantValue{
		NONSUBSUMED, SUBSUMED, INCONSISTENT, NIL
	}	

	/**
	 * change to the path where verifytga.exe and mutants folders are located
	 */
	private static String ecdarpath="C:\\Users\\Davide\\Desktop\\uppaal\\uppaal-tiga-0.17\\bin-Win32\\";
	private static String mutantpath="C:\\Users\\Davide\\"; //Dropbox\\Mutations_Experiments\\";
	private static String casestudy;

	public static void main(String[] args) throws IOException {
		//				casestudy="COFFEE";
		//				mutantpath+=casestudy+"2\\";
		//				runRefinementChecking(2);
		//				parseLog(2,false,parseViolationsLog(2));

		System.out.println("Path of Ecdar/Uppaal: "+ecdarpath);
		System.out.println("Folder containing mutants: "+mutantpath);

		if (args.length!=1) {
			System.out.println("-f  run Refinement Checking on first order mutants and  parse logs");
			System.out.println("-s  run Refinement Checking on second order mutants and parse logs");
			System.out.println("-pi  parse logs of first order mutants");
			System.out.println("-pe  parse logs of second order mutants");
			System.out.println("-a   run everything for all case studies");
			return;
		}

		Scanner scan = new Scanner(System.in); 


		if (args[0].contains("a")) {
			System.out.println("Everything. \n "
					+ "Warning! This operation may take a while. \n "
					+ "Press any key to continue");
			scan.nextLine();
			System.out.println("WARNING, CONFIRM AGAIN! \n ");
			scan.nextLine();
			String mutantpathback=mutantpath;
			for (int i=3; i<7;i++)
			{
				String press = ""+i;
				casestudy=(press.contains("1"))?"COFFEE"
						:(press.contains("2"))?"CAS"
								:(press.contains("3"))?"WFAS"
										:(press.contains("4"))?"ACCEL"
												:(press.contains("5"))?"MUTEX"
														:(press.contains("6"))?"HOTEL"
																:"";

				System.out.println(casestudy+" selected.");

				//				if (casestudy=="") {scan.close(); return;}
				//				mutantpath+=casestudy;
				//				mutantpath+="1\\";			
				//				runRefinementChecking(1);
				//				parseLog(false,parseViolationsLog(1));

				mutantpath=mutantpathback;
				mutantpath+=casestudy+"2\\";
				runRefinementChecking(2);
				parseLog(2,false,parseViolationsLog(2));

				mutantpath=mutantpathback;
			}
			scan.close();
			return;
		}
		System.out.println("Select Case study: \n"
				+ "Type 1 for COFFEE \n"
				+ "Type 2 for CAS \n"
				+ "Type 3 for WFAS \n" 
				+ "Type 4 for ACCEL \n"
				+ "Type 5 for MUTEX \n"
				+ "Type 6 for HOTEL \n");
		String press = scan.nextLine();
		casestudy=(press.contains("1"))?"COFFEE"
				:(press.contains("2"))?"CAS"
						:(press.contains("3"))?"WFAS"
								:(press.contains("4"))?"ACCEL"
										:(press.contains("5"))?"MUTEX"
												:(press.contains("6"))?"HOTEL"
														:"";

		System.out.println(casestudy+" selected.");
		if (casestudy=="") {scan.close(); return;}
		mutantpath+=casestudy;

		if (args[0].contains("f")) {
			System.out.println("First order mutants checking and parsing. \n "
					+ "Warning! This operation may take a while. \n "
					+ "Press any key to continue");
			scan.nextLine();
			mutantpath+="1\\";
			runRefinementChecking(1);
			parseLog(1,false,parseViolationsLog(1));
		}
		else if (args[0].contains("s")) {
			System.out.println("Second order mutants checking and parsing. \n "
					+ "Warning! This operation may take a while. \n"
					+ "Press any key to continue");
			scan.nextLine();
			mutantpath+="2\\";
			runRefinementChecking(2);
			parseLog(2,false,parseViolationsLog(2));
		}
		else if (args[0].contains("pi")){
			mutantpath+="1\\";
			parseLog(2,false,parseViolationsLog(1));
		}
		else if (args[0].contains("pe")){
			mutantpath+="2\\";
			parseLog(2,false,parseViolationsLog(2));
		}

		scan.close();
		System.out.println("Terminated successfully");
	}


	private static void parseLog(int orderint, boolean fineresults, Map<String,List<Integer>> mapViolations) throws IOException {
		//each mutation or couple of mutations (e.g. TAD-T_TMI) is an entry in the map
		Map<String, List<File>> mapfile = Files.list(Paths.get(mutantpath))
				.map(Path::toFile)
				.collect(Collectors.groupingBy(x->{
					String[] s = x.getName().split("\\.")[0].split("_");
					return (s.length==3)?s[1]: //first order
						(s.length==5)?s[1]+"_"+s[3]:""; //second order 
						//(s.length>1)?s[1]:"";
				} ));

		//computing for each log the number of subsumed, nonsubsumed and inconsistent mutants
		Map<String, Map<AppEcdar.MutantValue, Long>> parseMap = mapfile.entrySet().stream()
				.filter(x->x.getKey()!="")
				.collect(Collectors.toMap(Entry::getKey, e-> parseMutantLogToMap(e.getKey(),orderint))); 

		//		String order = (parseMap.entrySet().stream()
		//				.map(Entry::getKey)
		//				.findFirst()
		//				.orElse("")
		//				.contains("_"))?"second":"first";

		String order=(orderint==2)?"second":"first";

		String preamble ="\\begin{table}[tb]\r\n" + 
				"\\centering \r\n" + 
				"\\caption{\\label{tab:"+casestudy+orderint+"}"+casestudy+" "+ order +" order mutants results.}\r\n" + 
				"\\setlength{\\tabcolsep}{3pt}\r\n" + 
				"\\def\\arraystretch{1.2}\r\n" + 
				"\\scalebox{1}{\\begin{tabular}{|@{\\hskip1pt}l@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|}\r\n" + 
				"\\hline\r\n" + 
				"\\multirow{2}{*}{\\diagbox[trim=lr]%\r\n" + 
				"%\\diagbox[innerwidth=1cm,height=1.5cm]\r\n" + 
				"{\\small operator~}{\\small measure}}\r\n" + 
				"& \\multirow{2}{*}{\\small mutants} & \\multirow{2}{*}{$\\stackrel{\\text{\\small non-}}{\\text{\\small subsumed}}$} \r\n" + 
				"& \\multirow{2}{*}{\\small{subsumed}} \r\n" + 
				"& \\multirow{2}{*}{{\\small inconsistent}} \r\n" + 
				"& \\multirow{2}{*}{$\\stackrel{\\text{\\small violating}}{\\text{\\small guidelines}}$} &\r\n" + 
				"\\multirow{2}{*}{$\\frac{\\text{\\small violating}}{\\stackrel{\\text{\\small subsumed +}}{\\text{\\small inconsistent}}}$} \\\\ \r\n" + 
				"& & & & & & \\\\ \\hline \r\n" + 
				"";

		if (fineresults) 
		{
			//this part is not updated as coarse results

			//printing fine-grained results
			StringBuilder sbtex = new StringBuilder();

			sbtex.append(preamble); 

			for (Entry<String,Map<AppEcdar.MutantValue, Long>> e : parseMap.entrySet()) {
				sbtex.append(e.getKey()+" & & ");
				e.getValue().entrySet()
				.forEach(ee->{sbtex.append(ee.getValue()+"("+ee.getKey()+")  & "); });
				sbtex.append(" & \\\\"+System.lineSeparator());
			}

			sbtex.append("\\end{tabular}}"
					+ "\\end{table}");
			try (FileWriter logWritter = new FileWriter(casestudy+"_"+order+"-order_FinerResults.tex",false)){
				logWritter.write(sbtex.toString());
				logWritter.close();
			} 
		}

		//printing coarse results, e.g. grouping TAD-U and TAD-C

		StringBuilder sbtex = new StringBuilder();
		//	StringBuilder sbcsv = new StringBuilder();


		sbtex.append(preamble); 

		Map<String, List<Map<AppEcdar.MutantValue, Long>>>  groupedParseMap = parseMap.entrySet().stream()
				.collect(Collectors.groupingBy(e->
				{String[] s = e.getKey().split("_");
				return (s.length==1)?s[0].substring(0,3): //first order
					(s.length==2)?s[0].substring(0,3)+"-"+s[1].substring(0,3):"";}, //second order 
				//	return s[0].substring(0,3) + "-" + s[1].substring(0,3);}, 
				Collectors.mapping(Entry::getValue,Collectors.toList())));  


		DecimalFormat df = new DecimalFormat("#");
		Long tot_nonsubsumed=(long)0;
		Long tot_subsumed=(long)0;
		Long tot_inconsistent=(long)0;
		Long tot_violations=(long)0;

		for (Entry<String, List<Map<MutantValue, Long>>> e : groupedParseMap.entrySet())
		{
			Long nonsubsumed=e.getValue().stream()
					.map(m -> m.entrySet().stream()
							.filter(ee->ee.getKey().equals(MutantValue.NONSUBSUMED))
							.map(Entry::getValue)
							.collect(Collectors.summingLong(Long::longValue)))
					.collect(Collectors.summingLong(Long::longValue));

			Long subsumed=e.getValue().stream()
					.map(m -> m.entrySet().stream()
							.filter(ee->ee.getKey().equals(MutantValue.SUBSUMED))
							.map(Entry::getValue)
							.collect(Collectors.summingLong(Long::longValue)))
					.collect(Collectors.summingLong(Long::longValue));

			Long inconsistent=e.getValue().stream()
					.map(m -> m.entrySet().stream()
							.filter(ee->ee.getKey().equals(MutantValue.INCONSISTENT))
							.map(Entry::getValue)
							.collect(Collectors.summingLong(Long::longValue)))
					.collect(Collectors.summingLong(Long::longValue));


			Integer violations = mapViolations.get(e.getKey()).get(0);

			tot_nonsubsumed+=nonsubsumed;
			tot_subsumed+=subsumed;
			tot_inconsistent+=inconsistent;
			tot_violations+=violations;

			sbtex.append("\\texttt{"+e.getKey()+"} & " + (nonsubsumed+subsumed+inconsistent)  + " & ");
			sbtex.append(nonsubsumed+"   & ");
			sbtex.append((subsumed)+"  &");
			sbtex.append((inconsistent)+"  & ");
			sbtex.append(violations+"  & ");

			if (subsumed+inconsistent!=0)
				sbtex.append(df.format(((double)violations/(subsumed+inconsistent))*100) +"\\%  ");


			sbtex.append("  \\\\ \\hline"+System.lineSeparator());

			//			sbcsv.append("\\texttt{"+e.getKey()+"}, " + (nonsubsumed+subsumed+inconsistent)  + ", ");
			//			sbcsv.append(nonsubsumed+", ");
			//			sbcsv.append((subsumed)+", ");
			//			sbcsv.append((inconsistent)+", ");
			//			sbcsv.append(violations+", ");
			//			if (subsumed+inconsistent!=0)
			//				sbcsv.append(df.format(((double)violations/(subsumed+inconsistent))*100) +"%  ");
			//			sbcsv.append(System.lineSeparator());

		}

		sbtex.append("totals & " + (tot_nonsubsumed+tot_subsumed+tot_inconsistent)  + " & ");
		sbtex.append(tot_nonsubsumed+ " & ");
		sbtex.append((tot_subsumed)+"   & ");
		sbtex.append((tot_inconsistent)+"  & ");
		sbtex.append(tot_violations+" & ");

		if (tot_subsumed+tot_inconsistent!=0)
			sbtex.append(df.format(((double)tot_violations/(tot_subsumed+tot_inconsistent))*100) +"\\%  ");
		sbtex.append("  \\\\ \\hline"+System.lineSeparator());

		//		sbcsv.append("totals, " + (tot_nonsubsumed+tot_subsumed+tot_inconsistent)  + ", ");
		//		sbcsv.append(tot_nonsubsumed+", ");
		//		sbcsv.append(tot_subsumed+", ");
		//		sbcsv.append(tot_inconsistent+", ");
		//		sbcsv.append(tot_violations+", ");
		//		if (tot_subsumed+tot_inconsistent!=0)
		//			sbcsv.append(df.format(((double)tot_violations/(tot_subsumed+tot_inconsistent))*100) +"%  ");
		//
		//		sbcsv.append(System.lineSeparator());


		sbtex.append("\\end{tabular}}"
				+ "\\end{table}");

		order=(order=="second")?"Second":"First";

		try (FileWriter logWritter = new FileWriter(casestudy+"_"+order+"Order_CoarseResults.tex",false)){
			logWritter.write(sbtex.toString());
			logWritter.close();
		}

		//		try (FileWriter logWritter = new FileWriter(casestudy+"_"+order+"Order_CoarseResults.csv",false)){
		//			logWritter.write(sbcsv.toString());
		//			logWritter.close();
		//		} 
	}

	private static void runRefinementChecking(int order) {

		//each mutation or couple of mutations (e.g. TADTMI) is an entry in the map
		Map<String, List<File>> mapfile;
		try {
			mapfile = Files.list(Paths.get(mutantpath))
					.map(Path::toFile)
					.collect(Collectors.groupingBy(x->{
						String[] s = x.getName().split("\\.")[0].split("_");
						return (s.length==3)?s[1]: //first order
							(s.length==5)?s[1]+s[3]:""; //second order 
							//(s.length>1)?s[1]:"";
					} ));
		} catch (IOException e1) {
			System.out.println(e1.toString());
			e1.printStackTrace();
			return;
		}

		//all mutations are processed in parallel
		mapfile.entrySet().parallelStream()
		.filter(x->x.getKey()!="")
		.map(Entry::getValue)
		//.limit(2)  //just for debugging
		.forEach(list->
		{
			String[] temp = list.get(0).getName().split("\\.")[0].split("_");
			String logfilename = (temp.length==3)?temp[1]: //firstorder
				(temp.length==5)?temp[1]+"_"+temp[3]:"error"; //secondorder

				logfilename+="_log.txt";

				System.out.println("Start logging "+logfilename+" at "+LocalTime.now());

				StringBuilder sb = new StringBuilder();
				sb.append("-- "+LocalTime.now()+System.lineSeparator());
				for (File f : list) //all mutants belonging to that mutation 
				{
					String filename = f.getName().split("\\.")[0];
					sb.append(filename+System.lineSeparator());
					String cmd=String.format("cmd /c "+ecdarpath+
							"verifytga.exe %s "+mutantpath+"query.q",f.toString());

					Process process;
					try {
						process = Runtime.getRuntime().exec(cmd);
						BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
						String s;
						while ((s = stdInput.readLine()) != null) {
							sb.append(s);
						}
						sb.append("\n");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} 
				try (FileWriter logWriter = new FileWriter(System.getProperty("user.dir")+"\\experiments_reproducibility_EMSE\\"+casestudy+"\\"+logfilename,false)){
					logWriter.append(sb.toString());
				}catch (IOException e) {
					System.out.println("The folder "+System.getProperty("user.dir")+"\\experiments_reproducibility_EMSE\\"+casestudy+"\\  must be created first");
					System.out.println(e.toString());
					e.printStackTrace();
					return;
				}
		});
	}

	private static Map<AppEcdar.MutantValue, Long> parseMutantLogToMap(String mutant,int order)  {
		String file;
		try {
			file = new String (Files.readAllBytes( Paths.get(System.getProperty("user.dir")+"\\experiments_reproducibility_EMSE\\"+casestudy+"\\"+mutant+"_log.txt")));
		} catch (IOException e1) {
			System.out.println("The folder "+System.getProperty("user.dir")+"\\experiments_reproducibility_EMSE\\"+casestudy+"\\  must be created first");
			System.out.println(e1.toString());
			e1.printStackTrace();
			return null;
		}
		String[] mutants = file.split("mutant_");
		Map<String,List<MutantValue>> map = 
				IntStream.range(1,mutants.length) //discard first element
				.mapToObj(i->mutants[i])
				.collect(Collectors.groupingBy(s-> s.split(System.lineSeparator())[0], 
						Collectors.mapping(s->{return (s.contains("inconsistent")||s.contains("Inconsistent"))?MutantValue.INCONSISTENT
								:s.contains("NOT satisfied")?MutantValue.NONSUBSUMED
										:s.contains("satisfied")?MutantValue.SUBSUMED
												:MutantValue.NIL;},Collectors.toList())));
		//
		//				.collect(Collectors.toMap(s-> s.split("Options ")[0], 
		//						s->{return (s.contains("inconsistent")||s.contains("Inconsistent"))?MutantValue.INCONSISTENT
		//								:s.contains("NOT satisfied")?MutantValue.NONSUBSUMED
		//										:s.contains("satisfied")?MutantValue.SUBSUMED
		//												:MutantValue.NIL;}));

		//System.out.println(map.toString());

		Map<AppEcdar.MutantValue, Long> res = new HashMap<>();
		res.put(MutantValue.NONSUBSUMED, map.entrySet().stream()
				.filter(e->e.getValue().get(0).equals(MutantValue.NONSUBSUMED))
				.count());
		res.put(MutantValue.SUBSUMED, map.entrySet().stream()
				.filter(e->e.getValue().get(0).equals(MutantValue.SUBSUMED))
				.count());
		res.put(MutantValue.INCONSISTENT, map.entrySet().stream()
				.filter(e->e.getValue().get(0).equals(MutantValue.INCONSISTENT))
				.count());

		//System.out.println(res.toString());

		return res;
	}

	private static Map<String,List<Integer>> parseViolationsLog(int order) {
		if (order!=1 && order!=2)
			throw new IllegalArgumentException();
		String file;
		try {
			file = new String (Files.readAllBytes( Paths.get(mutantpath+casestudy+order+"_Violations.txt")));
		} catch (IOException e1) {
			System.out.println(e1.toString());
			e1.printStackTrace();
			return null;
		}

		String lines[]=file.split(System.lineSeparator());

		//System.out.println(Arrays.toString(lines));

		Map<String,List<Integer>> parsedMap=		
				Arrays.stream(lines)
				.map(x->x.split(","))
				.filter(x->x.length>1)//possible noise in data
				.collect(Collectors.toMap(x->x[0], x->
				Arrays.asList(Integer.parseInt(x[1]))));

		if (parsedMap.size()==0)
			throw new IllegalArgumentException();

		Map<String,List<List<Integer>>> groupedParsedMap=
				parsedMap
				.entrySet()
				.stream()
				.collect(Collectors.groupingBy(e->
				{String[] s = e.getKey().split("_");
				return (s.length==1)?s[0].substring(0,3): //first order
					(s.length==2)?s[0].substring(0,3)+"-"+s[1].substring(0,3):"";}, //second order 
				Collectors.mapping(Entry::getValue,Collectors.toList()))); 

		//System.out.println(groupedParsedMap.toString());

		Map<String,List<Integer>> result = new HashMap<>();
		for (Entry<String,List<List<Integer>>> e : groupedParsedMap.entrySet()) {
			List<Integer> temp = new ArrayList<>(Arrays.asList(0,0));
			for (List<Integer> li : e.getValue()) {
				temp.set(0, temp.get(0)+li.get(0));
			}
			result.put(e.getKey(), temp);
		}

		return result;
	}


}
