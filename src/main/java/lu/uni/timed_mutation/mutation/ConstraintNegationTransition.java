package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;

public class ConstraintNegationTransition implements Mutation {

	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		List<Node> transitions = m.transitionsWithGuard();
		Node transition = transitions.get(index);
		Node guard = m.guard(transition);
		
		//System.out.println(name() + "#" + (index) + " Negated guard of transition " + m.asText(transition));
		
		Model.negate(guard);
		
		return false;
	}

	@Override
	public String name() {
		return "CCN-T";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.transitionsWithGuard().size();
	}

}
