package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;

public class TransitionAddedUncontrollable implements Mutation {

	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		/*// Adding dummy action in system desc
		Node declaration = m.declaration();
		String oldDecl = declaration.getTextContent();
		String newDecl = oldDecl.replace(';', ',').concat(" dummy;");
		declaration.setTextContent(newDecl);
		String IOs = m.system().getTextContent();
		m.system().setTextContent(IOs.replaceAll("}", "").concat(", dummy!}"));*/
		
		Node template = m.transitionsWithActions().get(0).cloneNode(true);
		
		template.getAttributes().getNamedItem("controllable").setTextContent("false");
		Node transAction = m.action(template);
		transAction.setTextContent("dummyOutput!");
		
		m.addIO("dummyOutput", "!");

		Node transSource = Model.children(template, "source").get(0);
		Node transTarget = Model.children(template, "target").get(0);
		
		Node automaton = m.automata().get(0);
		
		List<Node> locations = m.locations(automaton);
		Node source = locations.get(index / m.locations(automaton).size());
		Node target = locations.get(index % m.locations(automaton).size());
		
		transSource.getAttributes().getNamedItem("ref").setTextContent(
				source.getAttributes().getNamedItem("id").getTextContent());
		
		transTarget.getAttributes().getNamedItem("ref").setTextContent(
				target.getAttributes().getNamedItem("id").getTextContent());

		automaton.appendChild(template);
				
		index++;
		
		//String src = Model.children(template, "source").get(0).getAttributes().getNamedItem("ref").getTextContent();
		//String tgt = Model.children(template, "target").get(0).getAttributes().getNamedItem("ref").getTextContent();
		//System.out.println(name() + "#" + (index) + " Added transition: " +m.asText(template));
		
		return false;
		
	}

	@Override
	public String name() {
		return "TAD-U";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.locations().size() * m.locations().size();
	}

}
