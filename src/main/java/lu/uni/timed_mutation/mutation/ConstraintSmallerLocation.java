package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;
import lu.uni.timed_mutation.model.Model.GuardKind;

public class ConstraintSmallerLocation implements Mutation {
	
	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		List<Node> locations = m.locationsWithInvariantGreaterThan(0);
		Node loc = locations.get(index);
		Node invariant = m.invariant(loc);
		
		boolean v = false;
		if((m.incomingTransitionsUncontrollable(loc) && (Model.guardkind(invariant) == GuardKind.LEQ || Model.guardkind(invariant) == GuardKind.LT)) ||
				(m.incomingTransitionsControllable(loc) && (Model.guardkind(invariant) == GuardKind.GEQ || Model.guardkind(invariant) == GuardKind.GT)) ) {
				v = true;
			}
		
		//count++;
		//String locId = loc.getAttributes().getNamedItem("id").getTextContent();
		//System.out.println(m.incomingTransitions(loc).size()+ " ? " + m.incomingTransitionsUncontrollable(loc));
		//System.out.println(name() +"#" + count + " Reduced invariant "+invariant.getTextContent()+" location " + locId + (v? " violating": ""));

		Model.reduceBound(invariant);
		
		return v;
		
	}

	@Override
	public String name() {
		return "CXS-L";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.locationsWithInvariantGreaterThan(0).size();
	}

}
