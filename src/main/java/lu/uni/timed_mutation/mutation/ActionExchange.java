package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;

public class ActionExchange implements Mutation {
	
	private int i = 0;
	private int j = 1;
	private int index = 0;

	@Override
	public boolean apply(Model m, int index) {
		
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		List<Node> transitions = m.transitions();

		Node ti = transitions.get(i % transitions.size());
		Node tj = transitions.get(j % transitions.size());

		Node tiActionNode = Model.findAttributedNode(ti, "synchronisation");
		Node tjActionNode = Model.findAttributedNode(tj, "synchronisation");

		String tiAction = tiActionNode.getTextContent();
		String tjAction = tjActionNode.getTextContent();

		tiActionNode.setTextContent(tjAction);
		tjActionNode.setTextContent(tiAction);
		
		if (tiAction.contains("?")) {
			tj.getAttributes().getNamedItem("controllable").setTextContent("true");
		} else {
			tj.getAttributes().getNamedItem("controllable").setTextContent("false");
		}
		
		if (tjAction.contains("?")) {
			ti.getAttributes().getNamedItem("controllable").setTextContent("true");
		} else {
			ti.getAttributes().getNamedItem("controllable").setTextContent("false");
		}
		
		do {
			j++;
			if (j >= transitions.size()) {
				j = 0;
				i++;
				if (i >= transitions.size()) {
					i = 0;
				}
			}
		} while (i == j);
		
		index++;
		
		//System.out.println(name() + "#" + (index) + " Switched action of transition " + m.asText(ti) + " with " + m.asText(tj));
		
		return false;
	}

	@Override
	public String name() {
		return "AEX";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.transitions().size() * m.transitions().size();
	}

}
