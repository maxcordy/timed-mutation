package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;
import lu.uni.timed_mutation.model.Model.GuardKind;

public class ConstraintLargerTransition implements Mutation {
	
	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		List<Node> transitions = m.transitionsWithGuard();	
		Node transition = transitions.get(index);
		Node guard = m.guard(transition);
		Node invariant = m.invariant(m.getSourceLocation(transition));
		
		boolean _not_to_ctrl_trans_with_x_leq_k = transition.getAttributes().getNamedItem("controllable").getTextContent().equals("true") && (Model.guardkind(guard) == GuardKind.LEQ || Model.guardkind(guard) == GuardKind.LT);
		boolean _not_to_unctrl_trans_with = transition.getAttributes().getNamedItem("controllable").getTextContent().equals("false");
		boolean _x_geq_k = Model.guardkind(guard) == GuardKind.GEQ || Model.guardkind(guard) == GuardKind.GT;
	    boolean _x_eq_k = Model.guardkind(guard) == GuardKind.EQ;
		boolean _source_invariant_x_leq_k = invariant != null? Model.guardkind(invariant) == GuardKind.LEQ || Model.guardkind(invariant) == GuardKind.LT : false;
		
		boolean violated = false;
		if( _not_to_ctrl_trans_with_x_leq_k ||
			( _not_to_unctrl_trans_with && (_x_geq_k || (_x_eq_k && _source_invariant_x_leq_k)))) {
				violated = true;
		}
		
		Model.raiseBound(guard);
		
		//System.out.println(name() + "#" + (index) + " Increased guard of transition " + m.asText(transition)+" with guard "+ guard.getTextContent()+" with invariant "+invariant.getTextContent()+" violated? "+violated);
		
		return violated;
	}

	@Override
	public String name() {
		return "CXL-T";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.transitionsWithGuard().size();
	}

}
