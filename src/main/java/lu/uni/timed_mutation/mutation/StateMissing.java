package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;

public class StateMissing implements Mutation {
	
	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		Node automaton = m.automata().get(0);
		List<Node> locations = m.locationsWithoutInitial(automaton);
		Node location = locations.get(index);
		
		boolean violated = false;
		if(m.incomingTransitionsUncontrollable(location)) {
			violated = true;
		}
		
		automaton.removeChild(location);
		m.removeTransitions(location);
		
		//System.out.println(name() +"#" + index + " Removed location " + locId + (violated? " violates the guidelines": " does not violate the guidelines"));
		
		return violated;
	}

	@Override
	public String name() {
		return "SMI";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.locationsWithoutInitial(m.automata().get(0)).size();
	}

}
