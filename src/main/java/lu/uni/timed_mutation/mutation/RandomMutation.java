package lu.uni.timed_mutation.mutation;

import java.util.List;
import java.util.Random;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;

public class RandomMutation implements Mutation {
	
	private Random rng = new Random();

	@Override
	public boolean apply(Model model, int index) {

		List<Node> locations = model.locations();
		List<Node> transitions = model.transitions();
		
		int rand = rng.nextInt(locations.size() + transitions.size());

		if (rand < locations.size()) {
			// Mutating location
			Node invariant = null;

			while (invariant == null) {

				Node location = pickRandomElement(locations);
				invariant = model.invariant(location);
			}

			System.out.println(invariant.getTextContent());
			replaceWithNewBound(invariant);
			System.out.println(invariant.getTextContent());

		} else {
			// Mutating transition
			Node guard = null;

			while (guard == null) {

				Node transition = pickRandomElement(transitions);
				guard = model.guard(transition);
			}

			System.out.println(guard.getTextContent());
			replaceWithNewBound(guard);
			System.out.println(guard.getTextContent());
			
		}
		
		return false;
		
	}
	
	private Node pickRandomElement(List<Node> l) {
		int rand = rng.nextInt(l.size());
		
		return l.get(rand);
	}
	
	private void replaceWithNewBound(Node constraint) {
		String asText = constraint.getTextContent();

		int index;
		if (asText.contains("==")) {
			index = asText.indexOf("==") + 2;
		} else if (asText.contains("=")) {
			index = asText.indexOf("=") + 1;
		} else if (asText.contains("<")) {
			index = asText.indexOf("<") + 1;
		} else if (asText.contains(">")) {
			index = asText.indexOf(">") + 1;
		} else {
			index = asText.indexOf(";") + 1;
		}
		String boundAsText = asText.substring(index);
		System.out.println(boundAsText);
		int bound = Integer.parseInt(boundAsText);

		int newBound = 0;
		Random rng = new Random();
		while (newBound <= bound) {
			newBound = rng.nextInt(bound * 2);
		}

		System.out.println(newBound);

		constraint.setTextContent(asText.substring(0, index) + newBound);
	}

	@Override
	public String name() {
		return "Random";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		// TODO Auto-generated method stub
		return 0;
	}
}
