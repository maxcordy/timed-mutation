package lu.uni.timed_mutation.mutation;

import lu.uni.timed_mutation.model.Model;

public interface Mutation {
	
	public boolean apply(Model m, int index);
	
	public String name();
	
	public int getNumberOfMutations(Model m);

}
