package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;
import lu.uni.timed_mutation.model.Model.GuardKind;

public class ConstraintSmallerTransition implements Mutation {
	
	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		List<Node> transitions = m.transitionsWithGuardGreaterThan(1);
		Node transition = transitions.get(index);
		Node guard = m.guard(transition);
		
		//System.out.println(name() + "#" + (index) + " Reduced guard of transition " + m.asText(transition));
		
		Model.reduceBound(guard);
		
		boolean violated = false;
		if((transition.getAttributes().getNamedItem("controllable").getTextContent().equals("true") && (Model.guardkind(guard) == GuardKind.GEQ || Model.guardkind(guard) == GuardKind.GT)) ||
			(transition.getAttributes().getNamedItem("controllable").getTextContent().equals("false") && (Model.guardkind(guard) == GuardKind.LEQ || Model.guardkind(guard) == GuardKind.LT))) {
			violated = true;
		}
		
		return violated;
	}

	@Override
	public String name() {
		return "CXS-T";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.transitionsWithGuardGreaterThan(1).size();
	}

}
