package lu.uni.timed_mutation.mutation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.model.Model;

public class TransitionDestinationExchange implements Mutation {
	
	private int t = 0;
	private int d = 0;
	private int index = 0;

	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		List<Node> transitions = m.transitions();
		List<Node> locations = m.locations();
		
		Node transition = transitions.get(t % transitions.size());
		

		index++;
		
		
		Node dest = Model.children(transition, "target").get(0).getAttributes().getNamedItem("ref");
		String oldDest = dest.getTextContent();
		int j = 0;
		for(Node location : locations) {
			if (j == d) {
				String locId = location.getAttributes().getNamedItem("id").getTextContent();
				if (locId.equals(oldDest)) {
					d++;
				} else {
					//System.out.print(name() + "#" + (index) + " Changed target of transition " + m.asText(transition) + " to ");	
					//System.out.println(locId);

					dest.setTextContent(locId);
					break;
				}
			}
			j++;
		}
		
		d++;
		if (d >= locations.size()) {
			d = 0;
			t++;
			if (t >= transitions.size()) {
				t = 0;
			}
		}
		
		return false;
	}

	@Override
	public String name() {
		return "TDE";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		// TODO Auto-generated method stub
		return 0;
	}

}
